#!/usr/bin/env python3

#
# checkpatch.py
#
# My very own and very dumb static patch checker
#

import re
import sys
import traceback
import types
import unidiff

# For python 3.6 (a.k.a. Ubuntu 18.04 LTS)
try:
	breakpoint
except:
        import pdb
        breakpoint = pdb.set_trace

def include_line_checker(line):
	'''Report on unexpected #include directives.

	Useful for scanning vendor trees for header files we believe are
	incorrectly organised.
	'''
	censored = (
		# Currently empty - populate as needed for different vendors
	)

	inc = line.includes()
	if inc:
		for c in censored:
			if c == inc:
				line.error("Includes " + inc)

def trapdoor_function_line_checker(line):
	'''Report on any usage of trapdoor functions.

	This is useful for scanning vendor trees to identify patches that make
	calls between otherwise unrelated components.
	'''
	censored = (
		# Currently empty - populate as needed for different vendors
	)

	for c in censored:
		if c in line.normalize():
			line.error("Uses trap door API: " + c)


def obsolete_api_line_checker(line):
	'''Report on any usage of obsolete functions.

	This is useful for scanning vendor trees to identify forward porting
	hazards.
	'''
	censored = (
		# TODO
	)

	for c in censored:
		if c in line.normalize():
			line.error("Uses obsolete API: " + c)

def missing_info_patch_checker(patch):
	'''Indicate patches with no patch_info field

	This is an important check because, at the time of writing, patch_info
	support is only available in the master branch (not yet released).
	'''
	if 'patch_info' not in patch[0].__dict__:
		patch.error('Patch has no patch_info (or unidiff library is too old)')

def guess_if_backported_patch_checker(patch):
	'''Look for hints this patch is an upstream backport.

	There are not any really good heuristics for this... so I've written
        a bad one instead!
	'''
	if 'patch_info' not in patch[0].__dict__:
		return

	header = patch[0].patch_info
	num_signed_off_by = 0

	# TODO: add automatic tag parsing into PatchSet
	for ln in header:
		if ln.startswith('Signed-off-by:'):
			num_signed_off_by += 1

	# If we have two or more Signed-off-by: then this is very likely
	# to be a backport.
	if num_signed_off_by >= 2:
		patch.warn("Suspected-backport: Has multiple Signed-off-by: tags")

        # TODO: Spot maintainers (e.g. those who occasionally write and commit
        #       their own patches) and use their names to identify single
        #       Signed-off-by: commits that are likely to be backports
        # Johannes Berg <johannes.berg@intel.com

def incremental_bsp_update_patch_checker(patch):
	'''Looks for patches that are merely incremental updates to files
	created by the BSP patch set (e.g. candidates for squashing).
	'''
	bsp_files = (
		# List the files created by an initial "BSP dump" patch here
		# and the checker will mark any patch that modified only these
		# files.
		#
		# You can point to single files or entire directory trees
		# (end a slash, for example, 'arch/arm/mach-imx/')
	)

	def is_bsp_file(fname):
		for bsp_fname in bsp_files:
			if bsp_fname.endswith('/'):
				if fname.startswith(bsp_fname):
					return True
			else:
				if fname == bsp_fname:
					return True

		return False

	purely_incremental = True
	partly_incremental = False

	for f in patch.added_files:
		if is_bsp_file(f.target_file[2:]):
			partly_incremental = True
		else:
			purely_incremental = False

	for f in patch.modified_files:
		src = f.source_file[2:]
		tgt = f.target_file[2:]

		# HACK: We assume [source|target]_file comments with 'a/' and
		#       'b/' respectively. Let's at least fail noisily!
		assert f.source_file != f.target_file
		assert src == tgt

		if is_bsp_file(tgt):
			partly_incremental = True
		else:
			purely_incremental = False

	if patch.removed_files:
		purely_incremental = False

	if purely_incremental:
		patch.warn("BSP-update: This patch only incrementally modifies the BSP")
	elif partly_incremental:
		patch.warn("Unclean-BSP-update: Patch modifies both BSP and not-BSP files")

class Line:
	CHECKERS = [globals()[c] for c in globals() if c.endswith('_line_checker')]
	RE_INCLUDE = re.compile('#\s*include\s+["<](?P<fname>[^">]+)[">].*')

	def __init__(self, fname, lineno, line, hunk):
		self.fname = fname
		self.lineno = lineno
		self.raw = line
		self.hunk = hunk

	def patch_set(self):
		return self.hunk.checkpatch_patched_file.checkpatch_patch_set

	def check(self, checkers=CHECKERS):
		for c in checkers:
			try:
				c(self)
			except:
				self.error("internal failure: {}".format(
						c.__name__))
				traceback.print_exc()
		self.patch_set().checkpatch_lines += 1

	def next(self, offset=1):
		return self.hunk.target_line(self.lineno + offset)[1:]

	def prev(self, offset=1):
		return self.hunk.target_line(self.lineno - offset)[1:]

	def normalize(self):
		return self.raw

	def includes(self):
		i = self.RE_INCLUDE.match(self.raw)
		if not i:
			return None
		return i.group('fname')

	def error(self, msg):
		self.patch_set.error(msg, fname=self.fame, lineno=self.lineno)

	def warn(self, msg):
		self.patch_set.warn(msg, fname=self.fame, lineno=self.lineno)

# Add a method to Hunks to look up lines based on their line number
unidiff.Hunk.target_line = types.MethodType(
    lambda self, lineno: self.target[lineno - self.target_start],
    unidiff.Hunk)

class PatchSet(unidiff.PatchSet):
	checkers = [globals()[c] for c in globals() if c.endswith('_patch_checker')]

	def check(self):
		self.checkpatch_modify = True
		self.checkpatch_errors = 0
		self.checkpatch_warnings = 0
		self.checkpatch_lines = 0

		for c in self.checkers:
			try:
				c(self)
			except:
				self.error("internal failure: {}".format(
						c.__name__))
				traceback.print_exc()
		for f in self:
			f.checkpatch_patch_set = self
			for hunk in f:
				hunk.checkpatch_patched_file = f

				for (offset, line) in enumerate(hunk.target):
					if not line.startswith('+'):
						continue
					line = line[1:]
					lineno = hunk.target_start + offset
					Line(f.path, lineno, line, hunk).check()

	def error(self, msg, fname=None, lineno='x'):
		if fname:
			fmsg = "{}:{}: error: {}".format(fname, lineno, msg)
		else:
			fmsg = "error: {}".format(msg)

		print(fmsg, file=sys.stderr)
		self.conditional_update(fmsg)

		self.checkpatch_errors += 1

	def warn(self, msg, fname=None, lineno='x'):
		if fname:
			fmsg = "{}:{}: warning: {}".format(fname, lineno, msg)
		else:
			fmsg = "warning: {}".format(msg)

		print(fmsg, file=sys.stderr)
		self.conditional_update(fmsg)
		self.checkpatch_warnings += 1

	def conditional_update(self, msg):
		'''Update the patch_info to include the checkpatch messages.'''
		if not self.checkpatch_modify:
			return

		if 'patch_info' not in self[0].__dict__:
			self[0].patch_info = unidiff.PatchInfo()
		self[0].patch_info.append(msg + '\n')

	def stats(self):
		return (self.checkpatch_errors, self.checkpatch_warnings,
		        self.checkpatch_lines)

def checkpatch(f, fname, writeback=False):
	# Python's built-in line splitting will convert 'A Line\r\r\n'
	# into *two* lines: 'A Line\r' and '\r\n'. This confuses unidiff
	# so we use a custom line splitting approach based exclusively on
	# \n .
	data = [ ln+'\n' for ln in f.read().split('\n') ]

	try:
		try:
			p = PatchSet(data)
		except UnicodeDecodeError:
			p = PatchSet(data, encoding='ISO-8859-1')
			print("{} contains UTF-8 decode errors\n".format(fname))
	except:
		print("{} cannot be parsed.\n".format(fname))
		traceback.print_exc()
		sys.exit(1)

	p.checkpatch_fname = fname
	p.check()
	stats = p.stats()
	print("total: {} errors, {} warnings, {} lines checked\n".format(
		*stats))
	if stats[0] or stats[1]:
		print("{} has style problems, please review.\n".format(fname))
	else:
		print("{} has no obvious style problems.\n".format(fname))

	if writeback:
		with open(fname+'.bak', 'w') as f:
			f.write(''.join(data))
		with open(fname, 'w') as f:
			print(p, file=f)

for fname in sys.argv[1:]:
	with open(fname, 'r') as f:
		checkpatch(f, fname)

if len(sys.argv[1:]) == 0:
	checkpatch(sys.stdin, "<stdin>")
