#!/bin/sh

#
# install-python3.sh
#
# This *should* be a carefully constructed script to ensure that the python3
# and python3-setuptools are reasonably up to date (outdated setuptools
# often leds to exceptions when trying to setup more recent modules).
# However... currently it is not carefully constructed, it is simply a
# quick hack that will only work on Debian Jessie.
#

# Trace execution and halt on first error
set -xe

# HACK: Ensure we have access to the backports
echo deb http://ftp.debian.org/debian jessie-backports main >> /etc/apt/sources.list

# Adopt the latest python3
apt-get update
apt-get install -t jessie-backports -y python3 python3-setuptools
