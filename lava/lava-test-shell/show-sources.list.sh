#!/bin/sh

for f in /etc/apt/sources.list /etc/apt/sources.list.d/*
do
	[ ! -f $f ] && continue

	echo ::::::::::::::
	echo $f
	echo ::::::::::::::
	cat $f
done

